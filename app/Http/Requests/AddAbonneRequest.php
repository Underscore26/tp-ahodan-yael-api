<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddAbonneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "nom" => "required|max:10",
            "prenoms" => "required|min:6",
            "email" => "required|email|unique:abonnes,email,",
            "contact" => "required",
        ];
    }
}
