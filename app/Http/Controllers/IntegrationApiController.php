<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;

use Illuminate\Http\Request;

class IntegrationApiController extends Controller
{
    public function integrationApi(){
        $response = Http::get("https://rawcdn.githack.com/kamikazechaser/administrative-divisions-db/master/api/CI.json");

        $data = json_decode($response->body());

        return response()->json([
            'langue' => "Français",
            'genre' => "Masculin",
            'region' => "Chretien",
            'pays' => "Côte d'Ivoire",
            'indicatif' => "CI",
            'internet' => true,
            'regions' => $data
        ]);
    }


}
