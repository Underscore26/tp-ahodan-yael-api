<?php

namespace App\Http\Controllers;

use App\Models\Abonne;
use App\Models\Compte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AbonneController extends Controller{

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //Retouner la liste des abonnées au format JSON
        return response()->json(Abonne::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){
        //On etablit des règles de validation pour les données de la requete
        $validate = Validator::make($request->all(), [
            "nom" => "required|max:10",
            "prenoms" => "required|min:6",
            "email" => "required|email|unique:abonnes,email,",
            "contact" => "required",
        ]);

        //On verifie les données de la requete
        if(!$validate->fails()) {
            //Si c'est ok, on crée un nouvelle abonné et on rempli ses champs
            $abonne = Abonne::create([
                "nom" => $request->get('nom'),
                "prenoms" => $request->get('prenoms'),
                "email" => $request->get('email'),
                "contact" => $request->get('contact')
            ]);

            //Si tout est bon, on retourne une message succès et la donnée ajoutée à la base de données
            return response()->json([
                'error' => false,
                'message' => "Opération éffectuée avec succès!!",
                'data' => $abonne
            ]);
        }else{
            //Si une donnée n'est pas valide, on retourne un message d'erreur
            return response()->json([
                "error" => true,
                "message" => $validate->errors()->first(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id){
        //On cherche l'abonné à partir de son id
        $abonne = Abonne::find($id);

        //On vérifie si l'abonné avec cet id existe dans la base de données
        if ($abonne) {
        //Si l'abonné existe, on retourne ses informations
            return response()->json([
                "error" => false,
                "message" => "Operation effectuée avec succes!!",
                "result" => $abonne
            ]);
        }else {
            //Si cet abonné n'existe pas, on affiche un message d'erreur
            return response()->json([
                'error' => true,
                'message' => "Désolé cette abonne n'existe pas dans notre base de données!"
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id){
        //On cherche l'abonné à partir de son id
        $abonne = Abonne::find($id);

        //On vérifie si l'abonné avec cet id existe dans la base de données
        if($abonne){
            //On etablit des règles de validation pour les données de la requete
            $validate = Validator::make($request->all(), [
                "nom" => "required|max:10",
                "prenoms" => "required|min:6",
                "email" => "required|email|unique:abonnes,email," . $abonne->id, //On ignore le champs email
                "contact" => "required",
                "active" => "required"
            ]);

            //Si une donnée n'est pas valide, on retourne un message d'erreur
            if($validate->fails()) {
                return response()->json([
                    "error" => true,
                    "message" => $validate->errors()->first(),
                ]);
            }

            //On modifie les champs à modifier
            $abonne->update([
                "nom" => $request->get('nom'),
                "prenoms" => $request->get('prenoms'),
                "email" => $request->get('email'),
                "contact" => $request->get('contact'),
                "active" => $request->get('active')
            ]);

            //Si tout est bon, on retourne une message succès et la donnée ajoutée à la base de données
            return response()->json([
                'error' => false,
                'message' => "Opération éffectuée avec succès!!",
                'data' => $abonne
            ]);
        }else {
            //Si cet abonné n'existe pas, on affiche un message d'erreur
            return response()->json([
                'error' => true,
                'message' => "Désolé cette abonne n'existe pas dans notre base de données!"
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id){
        //On cherche l'abonné à partir de son id
        $abonne = Abonne::find($id);

        //Si l'abonné existe dans notre base de données, on le supprime
        //sinon on retourne un message d'erreur
        if ($abonne) {
            $abonne->delete();
            return response()->json([
                'error' => false,
                'message'=> "Operation effectuée avec succes!!"
            ], 404);
        }
        return response()->json([
            'error' => true,
            'message' => "Désolé cette abonne n'existe pas dans notre base de données!"
        ], 404);
    }

    /**
     * Afficher la liste des abonnés avec leurs comptes
     */
    public function comptes(){
        return response()->json(Abonne::with('comptes')->get());
    }

    /**
     * Afficher la liste des abonnés avec leurs comptes
     */
    public function myComptes(string $id){
        $abonne = Abonne::with('comptes')->find($id);

        if($abonne){
            return response()->json([
                'error' => false,
                'data' => $abonne
            ]);
        }
        return response()->json([
            'error' => true,
            'message' => "Désolé cette abonne n'existe pas dans notre base de données!"
        ], 404);
    }

    /**
     * Lier un abonné et compte
     */
    public function liaison(Request $request){
        $abonneId = $request->get('abonneId');
        $compteId = $request->get('compteId');

        $abonne = Abonne::find($abonneId);
        $compte = Compte::find($compteId);

        if($compte && $abonne){
            $compte->abonne_id = $abonneId;
            $compte->save();

            return response()->json([
                'error' => false,
                'message' => "Succès"
            ]);
        }
        return response()->json([
            'error' => true,
            'message' => "Veuillez verifier que ces identifiants correspondent à un compte ou un abonné :)"
        ], 404);
    }

    /**
     * Afficher la statistique générale
     */
    public function statGenerale(){
        $nombre_de_compte = Compte::count();
        $nombre_abonne = Abonne::count();
        $total_montant = Compte::sum('montant');
        $montant_maximal = Compte::max('montant');
        $montant_minimal = Compte::min('montant');
        $moyenne_des_montant = Compte::avg('montant');

        return response()->json([
            'error' => false,
            'message' => "STATISTIQUE GENERALE",
            'data' => [
                'NOMBRE DE COMPTE' => $nombre_de_compte,
                "NOMBRE TOTAL D'ABONNE" => $nombre_abonne,
                'MONTANT TOTAL' => $total_montant. " FCFA",
                'MONTANT MAXIMAL' => $montant_maximal. " FCFA",
                'MONTANT MINIMAL' => $montant_minimal. " FCFA",
                'MONTANT MOYEN' => $moyenne_des_montant. " FCFA"
            ]
        ]);

    }

    /**
     * Afficher la statistique d'un abonné
     */
    public function abonneStat(string $id){
        $abonne = Abonne::find($id);

        if($abonne){
            // $nombre_de_compte = Compte::where('abonne_id', '=', $id)->count();
            $nombre_de_compte = $abonne->comptes()->count();
            $montant_total = $abonne->comptes()->sum('montant');
            $montant_maximal = $abonne->comptes()->max('montant');
            $montant_minimal = $abonne->comptes()->min('montant');
            $moyenne_des_montant = $abonne->comptes()->avg('montant');
            // dd($montant_total);
            return response()->json([
                'error' => false,
                'message' => "STATISTIQUE DE ".Str::upper($abonne->nom) . " ". Str::upper($abonne->prenoms),
                'data' => [
                    'NOMBRE DE COMPTE' => $nombre_de_compte,
                    'MONTANT TOTAL' => $montant_total. " FCFA",
                    'MONTANT MAXIMAL' => $montant_maximal. " FCFA",
                    'MONTANT MINIMAL' => $montant_minimal. " FCFA",
                    'MONTANT MOYEN' => $moyenne_des_montant. " FCFA"
                ]
            ]);
        }
        return response()->json([
            'error' => true,
            'message' => "Désolé cette abonne n'existe pas dans notre base de données!"
        ], 404);
    }
}
