<?php

namespace App\Http\Controllers;

use App\Models\Compte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class CompteController extends Controller{
    
    /**
     * Display a listing of the resource.
     * Si la rêquette contient un iban alors on affiche le compte
     * correspondant à cette iban
     */
    public function index(Request $request){
        if($request->has('iban')){
            $rib = Str::take($request->iban, -2);
            $compte = Compte::where('rib', '=', $rib)->get();
            return response()->json([
                'error' => false,
                'data' => $compte
            ]);
        }
        //Retouner la liste des comptes au format JSON
        return response()->json(Compte::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){
        //On etablit des règles de validation pour les données de la requete
        $validate = Validator::make($request->all(), [
            'libelle' => 'required|max:8',
            'description' => 'required|min:6',
            'montant' => 'required'
        ]);

        //On verifie les données de la requete
        if(!$validate->fails()){
            $compte = Compte::create([
                //Si c'est ok, on crée un nouveau compte pour un abonné
                'libelle' => $request->get('libelle'),
                'description' => $request->get('description'),
                'agence' => fake()->unique()->randomNumber(5, true),
                'banque' => 'CI'.fake()->unique()->randomNumber(3, true),
                'numero' => fake()->unique()->randomNumber(2, true).fake()->unique()->randomNumber(9, true),
                'rib' => fake()->unique()->randomNumber(2, true),
                'montant' => $request->get('montant'),
                'domiciliation' => $request->get('domicile')
            ]);

            //Si tout est bon, on retourne une message succès et la donnée ajoutée à la base de données
            return response()->json([
                'error' => false,
                'message' => "Le compte a été créé avec succès!",
                'data' => $compte
            ]);
        }else{
            //Si une donnée n'est pas valide, on retourne un message d'erreur
            return response()->json([
                'error' => true,
                "message" => $validate->errors()->first(),
            ], 404);
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id){
        //On recupère le compte à partir de son id
        $compte = Compte::find($id);

        //Vérifier que ce compte existe
        if($compte){
        //Si le compte existe, on retourne les détails du compte
            return response()->json([
                'error' => false,
                'message' => "Operation effectuée avec succes!!",
                'data' => $compte
            ]);
        }else{
        //Si le compte n'existe pas on retourne un message d'erreur
            return response()->json([
                'error' => true,
                'message' => "Ce compte n'existe pas!!"
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id){
        //On cherche le compte à partir de son id
        $compte = Compte::find($id);

        $validate = Validator::make($request->all(), [
            // 'libelle' => 'required|max:8,'.$compte->id,
            // 'description' => 'required|min:6,'.$compte->id,
            // 'montant' => 'required,'.$compte->id,
            // 'domiciliation' => 'required,'.$compte->id
        ]);

        //On va verifier si le compte existe vraimant
        if ($compte && !$validate->fails()) {
            $compte->update([
                // 'abonne_id' => $request->get('id_abonne'),
                'libelle' => $request->get('libelle'),
                'description' => $request->get('description'),
                'montant' => $request->get('montant'),
                'domiciliation' => $request->get('domicile')
            ]);

            return response()->json([
                'error' => false,
                'message' => "Le compte a été modifié avec succès!!",
                'data' => $compte
            ]);
        }else {
            //Si le compte n'existe pas on retourne un message d'erreur
            return response()->json([
                'error' => true,
                'message' => $validate->errors()->first(),
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id){
        $compte = Compte::find($id);

        if($compte){
            $compte->delete();
            return response()->json([
                'error' => false,
                'message' => 'Le compte a été supprimé avec succes!!'
            ], 404);
        }

        return response()->json([
            'error' => true,
            'message' => 'Ce compte est introuvable!!'
        ], 404);
    }
}
