<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Compte extends Model
{
    use HasFactory;

    //Autoriser ces champs à être rempli
    protected $fillable = [
        'abonne_id',
        'libelle',
        'description',
        'agence',
        'banque',
        'numero',
        'rib',
        'montant',
        'domiciliation'
    ];

    //Un compte appartient à un abonné
    public function abonne() : BelongsTo{
        return $this->belongsTo(Abonne::class);
    }
}
