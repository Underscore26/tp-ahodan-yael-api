<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Abonne extends Model
{
    use HasFactory;

    //Autoriser ces champs à être rempli
    protected $fillable = [
        'nom',
        'prenoms',
        'email',
        'contact',
        'active',
    ];

    //Un abonné peut avoir plusieur comptes
    public function comptes() : HasMany{
        return $this->hasMany(Compte::class);
    }
}
