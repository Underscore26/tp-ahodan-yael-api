<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Compte>
 */
class CompteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'libelle' => fake('fr_FR')->word(),
            'description' => fake('fr_FR')->sentence(),
            'agence' => fake()->randomNumber(5, true),
            'banque' => 'CI'.fake()->randomNumber(3, true),
            'numero' => fake()->unique()->randomNumber(2, true).fake()->unique()->randomNumber(9, true),
            'rib' => fake()->unique()->randomNumber(2, true),
            'montant' => fake()->numberBetween(0, 10000000000),
            'domiciliation' => fake()->streetAddress()
        ];
    }
}
