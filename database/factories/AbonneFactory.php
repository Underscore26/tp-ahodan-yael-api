<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Abonne>
 */
class AbonneFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nom' => fake('fr_FR')->lastName(),
            'prenoms' => fake('fr_FR')->firstName(),
            'email' => fake()->safeEmail(),
            'contact' => fake()->phoneNumber(),
            'active' => fake()->boolean(),
        ];
    }
}
