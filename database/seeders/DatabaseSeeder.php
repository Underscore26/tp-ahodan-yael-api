<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Abonne;
use App\Models\Compte;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        Abonne::factory(2)->create();
        Abonne::factory()->has(Compte::factory()->count(5))->count(4)->create();
        Abonne::factory()->has(Compte::factory()->count(7))->count(1)->create();
        Abonne::factory(1)->create();
        Abonne::factory()->has(Compte::factory()->count(1))->count(1)->create();
        Abonne::factory()->has(Compte::factory()->count(2))->count(1)->create();


        

    }
}
