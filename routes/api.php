<?php

use App\Http\Controllers\AbonneController;
use App\Http\Controllers\CompteController;
use App\Http\Controllers\IntegrationApiController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Creation des API basé sur l'architecture REST
Route::middleware('auth:sanctum')->prefix('v1')->group(function(){
    //----------------------------------------------Abonnés---------------------------------//

    //Afficher la liste abonnés
    Route::get('abonnes', [AbonneController::class, 'index']);

    //Afficher les détails d'un abonné
    // Route::get('/abonnes-{id}', [AbonneController::class, 'show']);
    Route::get('/abonne/{id}', [AbonneController::class, 'show'])->where('id', '[0-9]+');

    //Ajouter un abonné à la base de données
    Route::post('abonne', [AbonneController::class, 'store']);

    //Modifier les données d'un abonné
    Route::put('abonne/{id}', [AbonneController::class, 'update'])->where('id', '[0-9]+');

    //Supprimer un abonné
    Route::delete('abonne/{id}', [AbonneController::class, 'destroy'])->where('id', '[0-9]+');

    //----------------------------------------------Comptes---------------------------------//

    //Afficher la liste des comptes
    Route::get('comptes', [CompteController::class, 'index']);

    Route::get('comptes?iban=XXXX', [CompteController::class, 'index']);

    //Afficher les détails d'un compte
    Route::get('compte/{id}', [CompteController::class, 'show'])->where('id', '[0-9]+');

    //Ajouter un compte
    Route::post('comptes', [CompteController::class, 'store']);

    //Modifier le compte
    Route::put('compte/{id}', [CompteController::class, 'update'])->where('id', '[0-9]+');

    //Supprimer un compte
    Route::delete('compte/{id}', [CompteController::class, 'destroy'])->where('id', '[0-9]+');


    //----------------------------------------------Abonnés & Comptes---------------------------------//

    //Afficher la liste des abonnés avec leurs comptes
    Route::get('abonnes/comptes', [AbonneController::class, 'comptes']);

    //Afficher le détail abonnée avec compte
    Route::get('abonne/{id}/comptes', [AbonneController::class, 'myComptes']);

    //Lier un abonné et compte
    Route::post('liaisons', [AbonneController::class, 'liaison']);

    // //Afficher la statistique général
    Route::get('stats', [AbonneController::class, 'statGenerale']);

    // //Afficher la statistique d'un abonné
    Route::get('stats/abonnes/{id}', [AbonneController::class, 'abonneStat'])->where('id', '[0-9]+');

    //
    Route::get('personnes/random', [IntegrationApiController::class, 'integrationApi']);


});

Route::post('login', [UserController::class, 'index']);
